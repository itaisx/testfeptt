import axios from 'axios';
import config from '@/assets/config';

const actions = {
  GetFruits: async ({ commit }) => {
    await axios.get(`${config.serverURL}/`)
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          commit('getFruits', response);
        } else {
          console.log('RESPONSE FAIL');
        }
      })
      .catch(() => {
        console.log('RESPONSE FAIL');
      });
  },
  CreateFruits: async ({ commit }, payload) => {
    await axios.post(`${config.serverURL}/`, payload)
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          commit('addSuccess', response);
        } else {
          console.log('RESPONSE FAIL');
        }
      })
      .catch(() => {
        console.log('RESPONSE FAIL');
      });
  },
};

export default actions;
