import state from './fruitsStore/state';
import actions from './fruitsStore/actions';
import mutations from './fruitsStore/mutations';

const fruitsStore = {
  namespaced: true,
  state,
  mutations,
  actions,
};

export default fruitsStore;
