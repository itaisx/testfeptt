import Vue from "vue";
import Vuex from "vuex";

import fruitsStore from './fruitsStore';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    fruitsStore,
  }
});
